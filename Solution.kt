class Solution {
    fun twoSum(nums: IntArray, target: Int): IntArray {
        var result : IntArray = IntArray(2)
        val sizeIndex = nums.size-1
        for(i in 0..sizeIndex) {
            for(j in (i+1)..sizeIndex) {
                if(nums[i]+nums[j] == target){
                    result[0] = i
                    result[1] = j
                    return result
                }
            }
        }
        return result
    }
}